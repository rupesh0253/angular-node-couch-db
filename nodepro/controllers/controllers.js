const express = require('express');
const bodyParser = require('body-parser');
var router = express.Router();

const app = express();
app.use(bodyParser.json());


const dbfile = require('../db.js');
var couch = dbfile.met1;
var dbName = dbfile.met2;
var viewUrl = dbfile.met3;


router.get('/', function(req, res){
	res.send('welcome to crud');
});

router.get('/get', function(req, res){
	couch.get(dbName, viewUrl).then(
		function(data, headers, status){
			// console.log("hi.....",data.data.rows);
			// console.log("##############################----------data", data);
			// console.log("@@@@@@@@@@@@@@@@@@@@@@@@@@@@@----------headers", headers);
			// console.log("&&&&&&&&&&&&&&&&&&&&&&&&&&&&&---------------status", status);

			res.send({
				customers:data.data.rows
			}); 
		},
		function(err){
			res.send(err);
			console.log(err)
		});
});

router.post('/getbyid',function(req, res){
	const id = req.body.id;

	couch.get(dbName, id).then(
		function(data, headers, status){
			console.log("hi.....",data.data);
			
			
			res.send({
				customers:data.data
			}); 
		},
		function(err){
			res.send(err);
			console.log(err)
		});
});


router.post('/add', function(req, res){

	console.log("33", req.body);
	const name = req.body.name;
	const mobile = req.body.mobile;
	const emai = req.body.emai;
	const address = req.body.address;
	console.log("&&&", name);

	couch.uniqid().then(function(ids){
		const id = ids[0];

		console.log("id:", id);
		couch.insert(dbName,{
			_id:id,
			name:name,
			mobile:mobile,
			emai:emai,
			address:address,

		}).then(({data, headers, status}) => {
			console.log("daaaaa", data,headers,status)
			
			res.send({"message":"data added successfully"});
			}, err => {
    			res.send(err);
			});
	});

});


router.post('/update', function(req, res){
	console.log("7777");

	const id = req.body.id;
	const rev = req.body.rev;
	const name = req.body.name;
	const mobile = req.body.mobile;
	const address = req.body.address;

	console.log("@@@@@@@@@@@@@@@",id,rev,name,mobile,address);

	couch.update(dbName, {
		_id:id,
		_rev:rev,
		name:name,
		mobile:mobile,
		address:address

	}).then(
		function(data, headers, status){
			res.send('updated successfully');
		},
		function(err){
			res.send(err);
		});
});


router.post('/delete',function(req, res){
	const id = req.body.id;
	const rev = req.body.rev;
	console.log("iddddddd",id);
	couch.del(dbName, id, rev).then(
		function(data, headers, status){
			res.send({"message":"deleted successfully"});
	},
	function(err){
		res.send(err);
	});
});




module.exports = router;
const express = require('express');
const bodyParser = require('body-parser');
const cors = require('cors');

const couch  = require('./db.js');
var controllers  = require('./controllers/controllers.js');



const app = express();



app.use(bodyParser.json());
app.use(cors({ origin: 'http://localhost:4200'}))
app.use('/customers', controllers);


app.get('/', function(req, res){
	res.send('welcome to node project');
});

// server port
const port = process.env.PORT || 4600;


app.listen(port, (req, res) => {
    console.log(`RUNNING on port ${port}`);
});


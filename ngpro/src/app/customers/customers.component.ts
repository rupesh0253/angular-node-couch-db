import { Component, OnInit } from '@angular/core';
import { CustomerService } from '../Services/customer/customer.service';

@Component({
  selector: 'app-customers',
  templateUrl: './customers.component.html',
  styleUrls: ['./customers.component.css']
})
export class CustomersComponent implements OnInit {

  constructor(private custserv: CustomerService) { }

	  ngOnInit() {
	    console.log("component on in it");
	  	this.getAllData();

	  }

	public allCustomersData:any = [];
	getAllData(){
		console.log("component functionnn");
		this.custserv.getAllCust().subscribe( respdata =>{
			console.log("dattaaaa from nodejss ",respdata);
			this.allCustomersData = respdata.customers;
			console.log("dattaaaa from nodejss ",this.allCustomersData);

		})
		error => console.log("errorrr",error); 
	};

	public custom:any = {};
	addCustom(){

		console.log("add customer",this.custom);
		this.custserv.addCustomerr(this.custom).subscribe( respdata =>{
			console.log("@@@@",respdata);
		})
		error => console.log("error",error);

	};

	delcustom(custid,custrev){
		console.log("idddddddddddddd",custid,custrev);
		let data = {id:custid,rev:custrev}
		this.custserv.delCustomerr(data).subscribe( respdata =>{
			alert(respdata.message);
			console.log("*******",respdata);
		})
		error => console.log("errorrrrrrrrrr",error);
	}
}

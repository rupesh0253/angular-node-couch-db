import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class CustomerService {

  constructor(private http: HttpClient) { }
  readonly baseURL = "http://localhost:4600"

  getAllCust():Observable <any>{
  	console.log("serviceeeee");
  	return this.http.get(`${this.baseURL}/customers/get`)
  };

  addCustomerr(custom):Observable <any>{
  	console.log("hhhhhhhhhh", custom);
  	let url = `${this.baseURL}/customers/add`;
  	return this.http.post(url,custom);
  	
  };

  delCustomerr(data):Observable <any>{
  	console.log("hhhhhhhhhh", data);
  	let url = `${this.baseURL}/customers/delete`;
  	return this.http.post(url,data);
  	
  };
}
